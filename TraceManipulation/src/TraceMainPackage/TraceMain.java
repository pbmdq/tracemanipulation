package TraceMainPackage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class TraceMain {

	public static void main (String[] args){
		VistaTVTraceToEvent mySplitTract = new VistaTVTraceToEvent();
		//mySplitTract.splitDataLog();
		//mySplitTract.splitDataEPG();
		mySplitTract.mergeToOneFile();
		
	}
	public static class VistaTVTraceToEvent {
		private String EPGDir = "/Users/shengao/Documents/Research_data/VistaTV/Original/epg.csv";
		private String LogDir = "/Users/shengao/Documents/Research_data/VistaTV/Original/log.csv";
		
		private String eventEPGDir = "/Users/shengao/Documents/Research_data/VistaTV/Event/epg_sorted.csv";
		private String eventLogDir = "/Users/shengao/Documents/Research_data/VistaTV/Event/log_sorted.csv";
		
		
		public final SimpleDateFormat sdf 		= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		public void mergeToOneFile (){
			try{
				Scanner fileScannerLog = new Scanner(new File(eventLogDir));
				Scanner fileScannerEpg = new Scanner(new File(eventEPGDir));
				
				File mergeResults = new File("/Users/shengao/Documents/Research_data/VistaTV/Event/merged.csv");
				
				String inputStringLog = fileScannerLog.nextLine();
				String[]  afterSplitLog = inputStringLog.contains("\t")?inputStringLog.split("\t"):inputStringLog.split(" ");
				String inputStringEpg = fileScannerEpg.nextLine();
				String[]  afterSplitEpg = inputStringEpg.contains("\t")?inputStringEpg.split("\t"):inputStringEpg.split(" ");
				
				int i = 0;
				while(fileScannerLog.hasNextLine() && fileScannerEpg.hasNextLine()) {
					
					int timeStampStartLog		= Integer.parseInt(afterSplitLog[0]);
					int timeStampStartEPG		= Integer.parseInt(afterSplitEpg[0]);
					if(timeStampStartLog<timeStampStartEPG) {
						Files.append(inputStringLog+"\n", mergeResults, Charsets.UTF_8);
						inputStringLog = fileScannerLog.nextLine();
						afterSplitLog = inputStringLog.contains("\t")?inputStringLog.split("\t"):inputStringLog.split(" ");
						//System.out.println("log read:"+timeStampStartLog +"\t"+timeStampStartEPG);
					} else {
						Files.append(inputStringEpg+"\n", mergeResults, Charsets.UTF_8);
						inputStringEpg = fileScannerEpg.nextLine();
						afterSplitEpg = inputStringEpg.contains("\t")?inputStringEpg.split("\t"):inputStringEpg.split(" ");
						//System.out.println("Epg read:"+timeStampStartLog +"\t"+timeStampStartEPG);
					}
					/*
					i++;
					if(i > 100)
						break;
						*/
				}
				
			}catch (Exception e) {
				e.printStackTrace();
				//System.out.println(e.getStackTrace());
			}
			
		}
		public void splitDataLog (){
			try{
				int i = 0;
				File splitResults = new File("/Users/shengao/Documents/Research_data/VistaTV/Event/log.csv");
				Scanner fileScanner = new Scanner(new File(LogDir));
				ArrayList<String> tempResult = new ArrayList<String>();
				while(fileScanner.hasNextLine()) {
					String inputString = fileScanner.nextLine();
					String[]  afterSplit = inputString.contains("\t")?inputString.split("\t"):inputString.split(",");
					Date timeStampStart		= sdf.parse(afterSplit[1]);
					Date timeStampEnd		= sdf.parse(afterSplit[2]);
					String userName			= "<user:"+afterSplit[0]+">";
					String Channel  		= "<channel:"+afterSplit[3]+">";
					
					String startResult = timeStampStart.getTime()/1000+" "+timeStampStart.getTime()/1000+" "+userName+" <vistaTV:join> "+Channel+"\n";
					String endResult = timeStampEnd.getTime()/1000+" "+timeStampEnd.getTime()/1000+" "+userName+" <vistaTV:leave> "+Channel+"\n";
					tempResult.add(startResult);
					tempResult.add(endResult);
					
					//Files.append(startResult, splitResults, Charsets.UTF_8);
					//Files.append(endResult, splitResults, Charsets.UTF_8);
					
					//System.out.println(startResult);
					//System.out.println(startResult);
					
					/*
					i++;
					if(i > 100)
						break;
					*/
				
				}
				
				Collections.sort(tempResult);
				for(String temp: tempResult) {
					Files.append(temp, splitResults, Charsets.UTF_8);
				}
					
				
			}catch (Exception e) {
				System.out.println("error 2");
			}
		}
		
		public void splitDataEPG (){
			try{
				int i = 0;
				File splitResults = new File("/Users/shengao/Documents/Research_data/VistaTV/Event/epg.csv");
				Scanner fileScanner = new Scanner(new File(EPGDir));
				ArrayList<String> tempResult = new ArrayList<String>();
				while(fileScanner.hasNextLine()) {
					String inputString = fileScanner.nextLine();
					String[]  afterSplit = inputString.contains("\t")?inputString.split("\t"):inputString.split(",");
					Date timeStampStart		= sdf.parse(afterSplit[1]);
					Date timeStampEnd		= sdf.parse(afterSplit[2]);
					String userName			= "<programme:"+afterSplit[0]+">";
					String Channel  		= "<channel:"+afterSplit[3]+">";
					
					String startResult = timeStampStart.getTime()/1000+" "+timeStampStart.getTime()/1000+" "+userName+" <vistaTV:show> "+Channel+"\n";
					String endResult = timeStampEnd.getTime()/1000+" "+timeStampEnd.getTime()/1000+" "+userName+" <vistaTV:over> "+Channel+"\n";
					tempResult.add(startResult);
					tempResult.add(endResult);
					
					//Files.append(startResult, splitResults, Charsets.UTF_8);
					//Files.append(endResult, splitResults, Charsets.UTF_8);
					
					//System.out.println(startResult);
					//System.out.println(startResult);
					
					/*
					i++;
					if(i > 100)
						break;
					*/
				
				}
				
				Collections.sort(tempResult);
				for(String temp: tempResult) {
					Files.append(temp, splitResults, Charsets.UTF_8);
				}
					
				
			}catch (Exception e) {
				System.out.println("error 2");
			}
		}
		
		
		
	}
}
